﻿namespace Day03Tasks;

public class FindMissingElement
{
    public static int DiscoverMissingElement(int[] array)
    {
        if (array.Length == 1)
        {
            throw new ArgumentException("Array must have more than 1 element");
        }

        SortByMaxValue(array);
        for (int i = 0; i < array.Length - 1; i++)
        {
            if (!(array[i + 1] == array[i] + 1))
            {
                return array[i] + 1;
            }
        }

        return -1;
    }

    public static void SortByMaxValue(int[] array)
    {
        if (array.Length == 0)
        {
            return;
        }

        bool wasSorted = false;
        do
        {
            wasSorted = false;
            int maxValue = array[0];
            for (int j = 1; j < array.Length; j++)
            {
                if (maxValue > array[j])
                {
                    array[j - 1] = array[j];
                    array[j] = maxValue;
                    wasSorted = true;
                }
                else
                {
                    maxValue = array[j];
                }
            }
        } while (wasSorted);
    }


    //For MergeSort implementation
    private static int[] SortArray(int[] array)
    {
        if (array.Length == 1)
        {
            return array;
        }
        int[] newArray = SortSubArray(array);


        return newArray;
    }

    private static int[] SortSubArray(int[] array)
    {
        if (array.Length == 2)
        {
            if (array[0] > array[1])
            {
                int temp = array[0];
                array[0] = array[1];
                array[1] = temp;
            }
            return array;
        }

        int[] newArray = SortSubArray(array);
        for (int i = 0; i < newArray.Length / 2; i++)
        {
            for (int j = newArray.Length / 2; j < newArray.Length; j++)
            {
                if (newArray[i] > newArray[j])
                {
                    int temp = newArray[i];
                    newArray[i] = newArray[j];
                    newArray[j] = temp;
                }

            }
        }

        return newArray;
    }
}
