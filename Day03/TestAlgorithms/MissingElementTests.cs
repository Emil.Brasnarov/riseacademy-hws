using Day03Tasks;
namespace TestAlgorithms;

[TestClass]
public class MissingElementTests
{
    [TestMethod]
    public void TestSortByMaxValue()
    {
        //Assign
        int[] array = { 6, 4, 1, 3, 2, 5 };
        int[] Expected = { 1, 2, 3, 4, 5, 6 };

        //Act
        FindMissingElement.SortByMaxValue(array);

        //Assert
        CollectionAssert.AreEqual(Expected, array);
    }

    [TestMethod]
    public void TestDiscoverMissingElement()
    {
        //Assign
        int[] array = { 4, 2, 5, 7, 3 };
        int Expected = 6;

        //Act
        int result = FindMissingElement.DiscoverMissingElement(array);

        //Assert
        Assert.AreEqual(Expected, result);
    }

    [TestMethod]
    public void TestDiscoverMissingElementWhenArrayHasOneValue()
    {
        //Assemble
        int[] array = { 1 };
        string Expected = "Array must have more than 1 element";

        //Assert
        Assert.ThrowsException<ArgumentException>(() => FindMissingElement.DiscoverMissingElement(array), Expected);
    }

    [TestMethod]
    public void TestWhenNoElementIsMissing()
    {
        //Assign
        int[] array = { 4, 2, 5, 7, 3 , 6 };
        int Expected = -1;

        //Act
        int result = FindMissingElement.DiscoverMissingElement(array);

        //Assert
        Assert.AreEqual(Expected, result);
    }
}